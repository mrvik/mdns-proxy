//go:build systemd
// +build systemd

package main

import (
	"github.com/coreos/go-systemd/activation"
	"log"
	"net"
)

func getListeners() (tcpListener net.Listener, udpListener net.PacketConn, err error) {
	files := activation.Files(true)
	if err != nil {
		return
	}
	for _, f := range files {
		if conn, err := net.FilePacketConn(f); err == nil {
			log.Println("Got UDP listener from systemd")
			udpListener = conn
			f.Close()
		} else if lt, err := net.FileListener(f); err == nil {
			log.Println("Got TCP listener from systemd")
			tcpListener = lt
			f.Close()
		}
	}
	return
}
