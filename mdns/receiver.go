package mdns

import (
	"context"
	"log"
	"net"

	"github.com/miekg/dns"
	"golang.org/x/net/ipv4"
	"golang.org/x/net/ipv6"
)

func (rv *Resolver) joinMulticast() (err error) {
	if rv.listener, err = net.ListenPacket("udp", ":5300"); err != nil {
		log.Printf("Error on ListenPacket: %s\n", err)

		return
	}

	rv.conn4 = ipv4.NewPacketConn(rv.listener)
	rv.conn6 = ipv6.NewPacketConn(rv.listener)

	for _, iface := range rv.ifaces {
		var errored bool
		if err = rv.conn4.JoinGroup(iface, v4Multicast); err != nil {
			log.Printf("Error joining multicast with %s: %s\n", iface.Name, err)
			errored = true
		}
		if err = rv.conn6.JoinGroup(iface, v6Multicast); err != nil {
			log.Printf("Error joining multicast6 with %s: %s\n", iface.Name, err)
			errored = true
		}
		if !errored {
			rv.ifacesAdded = append(rv.ifacesAdded, iface)
		}
	}
	err = nil // Errors on join may not be fatal
	return
}

func (rv *Resolver) leaveMulticast() {
	for _, iface := range rv.ifacesAdded {
		if err := rv.conn4.LeaveGroup(iface, v4Multicast); err != nil {
			log.Printf("Error leaving interface %s: %s\n", iface.Name, err)
		}
		if err := rv.conn6.LeaveGroup(iface, v6Multicast); err != nil {
			log.Printf("Error leaving interface %s: %s\n", iface.Name, err)
		}
	}
}

func (rv *Resolver) receiveResponse(ctx context.Context) (response *dns.Msg, err error) {
	var res *responsePacket
	select {
	case res = <-rv.incoming:
	case <-ctx.Done():
		err = ctx.Err()
		return
	}
	if res.err != nil {
		log.Printf("Error reading from listener: %s\n", res.err)
		err = res.err
		return
	}
	response = new(dns.Msg)
	err = response.Unpack(res.bytes)
	return
}

func (rv *Resolver) readerRoutine(ctx context.Context) {
	var buf [1500]byte
	for {
		select {
		case <-ctx.Done():
			return
		default:
		}
		n, _, err := rv.listener.ReadFrom(buf[:])
		select {
		case rv.incoming <- &responsePacket{
			bytes: buf[:n],
			err:   err,
		}:
		default:
		}
	}
}
