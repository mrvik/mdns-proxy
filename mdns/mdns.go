// Package mdns resolves hostnames using mdns.
// See, we query A and AAAA records instead of looking for PTR and SRV.
// It wasn't that hard!
package mdns

import (
	"context"
	"fmt"
	"log"
	"net"
	"strings"

	"golang.org/x/net/ipv4"
	"golang.org/x/net/ipv6"

	"github.com/miekg/dns"
)

const (
	mdnsPort = 5353
)

var (
	v4Multicast = &net.UDPAddr{
		IP:   net.IPv4(224, 0, 0, 251),
		Port: mdnsPort,
	}
	v6Multicast = &net.UDPAddr{
		IP:   net.ParseIP("ff02::fb"),
		Port: mdnsPort,
	}
)

type responsePacket struct {
	bytes []byte
	err   error
}

type Resolver struct {
	ifaces      []*net.Interface
	ifacesAdded []*net.Interface

	conn4 *ipv4.PacketConn
	conn6 *ipv6.PacketConn

	listener net.PacketConn
	incoming chan *responsePacket
}

func NewResolver(ctx context.Context) (rv *Resolver, err error) {
	ifaces, err := multicastInterfaces()
	if err != nil {
		return
	}
	rv = &Resolver{
		ifaces:   ifaces,
		incoming: make(chan *responsePacket, 2),
	}
	err = rv.joinMulticast()
	go rv.readerRoutine(ctx)
	return
}

func (rv *Resolver) ResolveHost(ctx context.Context, hostname string, qtype uint16) (response *dns.Msg, err error) {
	msg := dns.Msg{
		Question: []dns.Question{
			{
				Name:   hostname,
				Qtype:  qtype,
				Qclass: dns.ClassINET,
			},
		},
	}
	msg.RecursionDesired = false
	packed, err := msg.Pack()
	if err != nil {
		return
	}

	if err := rv.sendRequest(packed); err != nil {
		return nil, err
	}

	// Now we wait for host to be resolved
	response, err = rv.receiveResponse(ctx)
	return
}

func (rv *Resolver) sendRequest(wireData []byte) error {
	errors := make(MultiError, 0, len(rv.ifaces)*2)

	for _, iface := range rv.ifacesAdded {
		v4, v6 := findAddresses(*iface)
		pkg4 := ipv4.ControlMessage{
			Src:     v4,
			Dst:     v4Multicast.IP,
			IfIndex: iface.Index,
		}
		pkg6 := ipv6.ControlMessage{
			Src:     v6,
			Dst:     v6Multicast.IP,
			IfIndex: iface.Index,
		}

		log.Printf("Emitting request on interface %s", iface.Name)

		if v4 != nil {
			if _, err := rv.conn4.WriteTo(wireData, &pkg4, v4Multicast); err != nil {
				errors = append(errors, fmt.Errorf("write v4 packet to %s: %w", iface.Name, err))
			}
		}

		if v6 != nil {
			if _, err := rv.conn6.WriteTo(wireData, &pkg6, v6Multicast); err != nil {
				errors = append(errors, fmt.Errorf("write v6 packet to %s: %w", iface.Name, err))
			}
		}
	}

	switch el := len(errors); {
	case el == 0:
		return nil
	case el < len(rv.ifaces)*2:
		log.Printf("send requests: %s", errors.Error())
		return nil
	}

	return errors
}

func (rv *Resolver) Stop() {
	rv.leaveMulticast()
}

type MultiError []error

func (m MultiError) Error() string {
	es := make([]string, len(m))

	for i, e := range m {
		es[i] = e.Error()
	}

	return fmt.Sprintf("multiple errors: [%s]", strings.Join(es, ","))
}

func findAddresses(iface net.Interface) (v4, v6 net.IP) {
	slice, _ := iface.Addrs()

	for _, addr := range slice {
		ip := castIP(addr)

		if s := ip.To4(); s != nil {
			v4 = s
		} else if s6 := ip.To16(); s6 != nil {
			v6 = s6
		}
	}

	return
}

func castIP(n net.Addr) net.IP {
	switch v := n.(type) {
	case *net.IPNet:
		return v.IP
	case *net.IPAddr:
		return v.IP
	}

	return nil
}
