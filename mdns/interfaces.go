package mdns

import (
	"log"
	"net"
)

// Get a list of interfaces attached and supporting multicast
func multicastInterfaces() (ifaces []*net.Interface, err error) {
	allInterfaces, err := net.Interfaces()
	if err != nil {
		return
	}

	ifaces = make([]*net.Interface, 0, len(allInterfaces))

	for _, iface := range allInterfaces {
		if (iface.Flags&net.FlagUp) != net.FlagUp || (iface.Flags&net.FlagMulticast) != net.FlagMulticast {
			continue
		}

		log.Printf("Interface %s is available\n", iface.Name)

		ifaces = append(ifaces, copyIface(iface))
	}
	if len(ifaces) == 0 {
		log.Fatal("No suitable interfaces for multicast")
	}
	return
}

func copyIface(s net.Interface) *net.Interface {
	r := new(net.Interface)
	*r = s

	return r
}
