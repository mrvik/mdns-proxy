package main

import (
	"context"
	"log"
	"time"

	"github.com/miekg/dns"
	"gitlab.com/mrvik/mdns-proxy/mdns"
)

type dnsResponder struct {
	ctx          context.Context
	mdnsResolver *mdns.Resolver
}

func newResponder(ctx context.Context) *dnsResponder {
	resolver, err := mdns.NewResolver(ctx)
	if err != nil {
		panic(err)
	}

	return &dnsResponder{
		ctx:          ctx,
		mdnsResolver: resolver,
	}
}

func (rp *dnsResponder) ServeDNS(rw dns.ResponseWriter, req *dns.Msg) {
	defer rw.Close()

	var (
		q      = req.Question[0]
		err    error
		answer []dns.RR
		rcode  = dns.RcodeSuccess
	)

	log.Printf("Question from client %s: %+v\n", rw.RemoteAddr(), q)

	if answer, err = rp.mdnsLookup(q.Name, q.Qtype); err != nil {
		log.Printf("Error on lookup: %s\n", err)
		rcode = dns.RcodeServerFailure
	}

	if len(answer) == 0 {
		rcode = dns.RcodeNameError
	}

	log.Printf("Responding with %s %+v\n", dns.RcodeToString[rcode], answer)
	req.Answer, req.Rcode = answer, rcode
	rw.WriteMsg(req)
}

func (rp *dnsResponder) mdnsLookup(service string, queryType uint16) (records []dns.RR, err error) {
	ctx, done := context.WithTimeout(rp.ctx, time.Second*10)
	defer done()
	res, err := rp.mdnsResolver.ResolveHost(ctx, service, queryType)
	if res != nil {
		records = res.Answer
	}
	return
}

func (rp *dnsResponder) Stop() {
	rp.mdnsResolver.Stop()
}
