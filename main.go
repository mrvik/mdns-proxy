package main

import (
	"context"
	"log"
	"os"
	"os/signal"
	"sync"
	"syscall"
	"time"

	"github.com/miekg/dns"
)

func main() {
	ctx, cancel := context.WithCancel(context.Background())
	//Generate ServeMux
	responder := newResponder(ctx)
	defer responder.Stop()
	mux := dns.NewServeMux()
	mux.Handle("local.", responder)
	mux.Handle("in-addr.arpa", responder)
	tcp, udp, err := getListeners()
	if err != nil {
		panic(err)
	}
	if tcp == nil && udp == nil {
		panic("No sockets to listen on")
	}
	var wg sync.WaitGroup
	if tcp != nil {
		wg.Add(1)
		server := dns.Server{
			Handler:  mux,
			Listener: tcp,
		}
		go startServer(ctx, &server, &wg)
	}
	if udp != nil {
		wg.Add(1)
		server := dns.Server{
			Handler:    mux,
			PacketConn: udp,
		}
		go startServer(ctx, &server, &wg)
	}
	go listenSignals(ctx, cancel)
	wg.Wait()
}

func startServer(ctx context.Context, server *dns.Server, wg *sync.WaitGroup) {
	defer wg.Done()
	serverDone := make(chan error, 1)
	go func() {
		serverDone <- server.ActivateAndServe() //This method claims to be for systemd compat, but if we set the listeners manually we must use it anyway
	}()
	var err error
	select {
	case err = <-serverDone:
	case <-ctx.Done():
		tctx, done := context.WithTimeout(context.Background(), time.Second*5)
		defer done()
		err = server.ShutdownContext(tctx)
	}
	log.Println("Server stopped")
	if err != nil {
		panic(err)
	}
}

func listenSignals(ctx context.Context, cancelFn context.CancelFunc) {
	defer cancelFn()
	signalChannel := make(chan os.Signal, 2)
	defer close(signalChannel)
	signal.Notify(signalChannel, syscall.SIGINT, syscall.SIGTERM)
	select {
	case <-signalChannel:
	case <-ctx.Done():
	}
}
