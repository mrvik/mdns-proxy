//go:build !systemd
// +build !systemd

package main

import (
	"flag"
	"fmt"
	"log"
	"net"
)

func getListeners() (tcp net.Listener, udp net.PacketConn, err error) {
	var (
		listenAddress string
		listenPort    int
		listenNet     string
	)
	// Flag parsing
	flag.StringVar(&listenAddress, "address", "", "Listen address. Defaults to all interfaces")
	flag.IntVar(&listenPort, "port", 53, "Listen port. Defaults to 53")
	flag.StringVar(&listenNet, "net", "both", "Listen on tcp, udp or both")
	flag.Parse()
	addr := fmt.Sprintf("%s:%d", listenAddress, listenPort)
	log.Printf("Listening on addr %s\n", addr)
	if listenNet == "both" || listenNet == "tcp" {
		log.Println("Listening on TCP transport")
		tcp, err = net.Listen("tcp", addr)
		if err != nil {
			return
		}
	}
	if listenNet == "both" || listenNet == "udp" {
		log.Println("Listening on UDP transport")
		udp, err = net.ListenPacket("udp", addr)
		if err != nil {
			return
		}
	}
	return
}
