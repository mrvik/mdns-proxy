package main

import (
	"context"
	"fmt"
	"os"
	"strings"
	"time"

	"gitlab.com/mrvik/mdns-proxy/mdns"
)

func main() {
	ctx := context.Background()
	resolver, err := mdns.NewResolver(ctx)
	if err != nil {
		panic(err)
	}
	defer resolver.Stop()
	for _, hostname := range os.Args[1:] {
		addr, err := resolve(ctx, resolver, hostname)
		if err != nil {
			fmt.Printf("Error resolving %s: %s\n", hostname, err)
			continue
		}
		fmt.Printf("Query for %s returned\n%s\n", hostname, addr)
	}
}

func resolve(ctx context.Context, resolver *mdns.Resolver, hostname string) (addr string, err error) {
	ctx, done := context.WithTimeout(ctx, time.Second*10)
	defer done()
	res, err := resolver.ResolveHost(ctx, hostname+".", 1) //A
	if err == nil {
		if res == nil || len(res.Answer) == 0 {
			addr = "No IP for host " + hostname
			return
		}
		var builder strings.Builder
		for _, addr := range res.Answer {
			builder.WriteString(addr.String())
		}
		addr = builder.String()
	}
	return
}
